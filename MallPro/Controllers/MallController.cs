﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MallPro.Models;
using static MallPro.Models.ViewModel;

namespace MallPro.Controllers
{
    public class MallController : Controller
    {
        MallManagementEntities1 db = new MallManagementEntities1();


        public ActionResult DetailsAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DetailsAdd(MallOwnerViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {
                MallOwnerDetailsTbl NewAdd = new MallOwnerDetailsTbl();
                NewAdd.MallOwnerID = DetailsPost.MallOwnerID;
                NewAdd.OwnerName = DetailsPost.OwnerName;
                NewAdd.Address = DetailsPost.Address;
                NewAdd.Email = DetailsPost.Email;

                db.MallOwnerDetailsTbls.Add(NewAdd);
                db.SaveChanges();
                return RedirectToAction("DetailsAdd");
            }
            return View(DetailsPost);

        }
    




        /*    public ActionResult Category()
            {
                var Category = db.CategoryTbls.FirstOrDefault();
                CategoryViewModel Showcategory = new CategoryViewModel();
                //  Showcategory.CategoryID = Category.CategoryID;
                //  Showcategory.CategoryType = Category.CategoryType;
                return View(Showcategory);

            }

            public ActionResult Index()
            {
                return View();
            }*/
    }
}
