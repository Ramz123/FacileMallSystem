//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MallPro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ShopInterestRequestTbl
    {
        public int ShopInterestRequestID { get; set; }
        public string ExpressedByName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public Nullable<System.DateTime> DateExpressed { get; set; }
        public string Message { get; set; }
        public Nullable<int> ShopID { get; set; }
    }
}
