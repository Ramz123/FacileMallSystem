﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace MallPro.Models
{
    public class ViewModel
    {

        public class MallOwnerViewModel
        {
           
            [Display(Name = "Mall OwnerID")]
            public string MallOwnerID { get; set; }
            [Display(Name = "Owner Name")]
            public string OwnerName { get; set; }
            [Display(Name = "Address")]
            public string Address { get; set; }
            [Display(Name = "Email")]
            public string Email { get; set; }



            /*public class CategoryViewModel
            {
                //public int? CategoryID { get; set; }

                [Required]
                [Display(Name = "Category Name")]
                public string CategoryName { get; set; }
            }*/

        }
    }
}






