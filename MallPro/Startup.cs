﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MallPro.Startup))]
namespace MallPro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
